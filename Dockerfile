FROM atlas/analysisbase:21.2.52

COPY . /code

WORKDIR /code
RUN source ~/release_setup.sh && \
    sudo chown -R atlas /code && \
    g++ -o helloworld helloworld.cxx
    